# Deep Look

Deep Lock (em português Bloqueio Profundo) seja talvez algo inútil para computadores pessoais, mas altamente útil para facilitar a administração de computadores públicos como: faculdades, escolas públicas, lan houses e outros.

A aplicação utiliza as seguintes tecnologias:
* Electron
* HTML
* CSS
* Bootstrap (materialize)

![](deep-lock/deep-lock_1.0.0_all/opt/deep-lock/resources/app/images/mascote.svg)


Uma ferramenta que permite “congelar” o diretório pessoal de um ou mais usuários e fazer com que alterações feitas na interface e nos arquivos sejam nulas e voltem ao padrão estabelecido após uma reinicialização.uma ferramenta nacional oriunda do projeto DuZeru GNU/Linux.
O Deep Lock esta disponível no formato .deb para Debian (DuZeru), Ubuntu, Linux Mint e seus derivados, assim como seu código fonte.
Para instalar no DuZeru execute o comando:
```
sudo apt-get update && sudo apt-get install deep-lock
```

Para Derivados do Debian como Ubuntu, Mint, Elementary, deve ser efetuado o download no link:
[https://gitlab.com/duzeru/deep-lock/-/jobs/463172729/artifacts/raw/deep-lock_1.0.0_all.deb](https://gitlab.com/duzeru/deep-lock/-/jobs/463172729/artifacts/raw/deep-lock_1.0.0_all.deb)

Após a intalação, não se conesgue congelar os usuários devido não haver atualmente o gksu no Ubuntu, deve ser executado para abrir o aplicativo o comando:
```
sudo deep-lock
```

Breve será corrigido a funcionalidade no Ubuntu, mint e outros derivados do Debian.

Agradecimentos especiais ao @renerdias por dar o pontapé inicial para o planejamento, execução e lançamento do App.
